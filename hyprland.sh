# Display manager

#sudo pacman -S gdm
#sudo systemctl enable gdm

# Aur manager

#git clone https://aur.archlinux.org/paru.git
#cd paru
#makepkg -si
#cd ..
#sudo rm -R paru

# Basic application

paru -S firefox alacritty unzip zsh telegram-desktop brightnessctl neovim bat neofetch tree valgrind wget python-pip mesa exa feh bluez bluez-utils cpupower-gui diagnostic-languageserver discord discord-canary docker docker-compose dunst efibootmgr electron htop gtop man man-pages neofetch neovim pavucontrol swaylock-effects nvidia-dkms tetrio-desktop thunar ttf-jetbrains-mono ttf-jetbrains-mono-nerd webcord weston wf-recorder wofi zeal clion clion-gdb 

#chsh -s /bin/zsh

# Hyprland install

# Manual
# git clone --recursive https://github.com/hyprwm/Hyprland
# cd Hyprland
# sudo make install
#
# ... TODO
# 
# OR

paru -S hyprland-nvidia hyprpaper hyprpicker waybar-hyprland wofi wlogout-git dunst imw waylock

# Screen charing on hyprland

sudo pacman -S xdg-desktop-portal-wlr
# check: pacman -Q | grep xdg-desktop-portal-          # make sure you don't have any other xdg-desktop
sudo pacman -S grim slurp

# Add to end of hyprland.conf
# exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
#

# FONT
sudo mkdir /usr/share/fonts/nerd-fonts
cd /usr/share/fonts/nerd-fonts
sudo curl -LJO https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip
sudo unzip Hack.zip
sudo rm Hack.zip
fc-cache -vf

git clone https://gitlab.com/isserov25/dotfiles2.0.git 

mv ~/dotfiles2.0/alacritty ~/.config 
mv ~/dotfiles2.0/hypr ~/.config
mv ~/dotfiles2.0/maki ~/.config
mv ~/dotfiles2.0/nvim ~/.config 
mv ~/dotfiles2.0/tetrio-desktop ~/.config
mv ~/dotfiles2.0/waybar ~/.config 
mv ~/dotfiles2.0/wlogout ~/.config 
mv ~/dotfiles2.0/wofi ~/.config


