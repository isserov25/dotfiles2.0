#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

dir="~/.config/waybar/scripts/rofi"
uptime=$(uptime -p | sed -e 's/up //g')

rofi_command="rofi -no-config -theme $dir/powermenu.rasi"

# Options
shutdown="⏻ Shutdown"
reboot=" Restart"
lock=" Lock"
suspend="  Sleep"
logout=" Logout"
# Confirmation
confirm_exit() {
	rofi -dmenu\
        -no-config\
		-i\
		-no-fixed-num-lines\
		-p "Are You Sure? : "\
		-theme $dir/confirm.rasi
}

# Message
msg() {
	rofi -no-config -theme "$dir/message.rasi" -e "Available Options  -  yes / y / no / n"
}

# Variable passed to rofi
options="$lock\n$suspend\n$logout\n$reboot\n$shutdown"

chosen="$(echo -e "$options" | $rofi_command -p "Uptime: $uptime" -dmenu -selected-row 0)"
case $chosen in
    $shutdown)
		ans=$(confirm_exit &)
		if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
			systemctl poweroff
		elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
			exit 0
        else
			msg
        fi
        ;;
    $reboot)
		ans=$(confirm_exit &)
		if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
			systemctl reboot
		elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
			exit 0
        else
			msg
        fi
        ;;
    $lock)
		# if [[ -f /usr/bin/i3lock ]]; then
		# 	sh ~/.config/i3/scripts/blur-lock
		# elif [[ -f /usr/bin/betterlockscreen ]]; then
		# 	betterlockscreen -l
		# fi
      exec swaylock \
--screenshots \
--indicator \
--clock \
--inside-wrong-color f38ba8  \
--ring-wrong-color 11111b  \
--inside-clear-color a6e3a1 \
--ring-clear-color 11111b \
--inside-ver-color 89b4fa \
--ring-ver-color 11111b \
--text-color  f5c2e7 \
--indicator-radius 80 \
--indicator-thickness 5 \
--effect-blur 10x7 \
--effect-vignette 0.2:0.2 \
--ring-color 11111b \
--key-hl-color f5c2e7 \
--line-color 313244 \
--inside-color 0011111b \
--separator-color 00000000 \
--fade-in 0.1 &
        ;;
    $suspend)
		ans=$(confirm_exit &)
		if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
			mpc -q pause
			amixer set Master mute
			systemctl suspend
		elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
			exit 0
        else
			msg
        fi
        ;;
    $logout)
		ans=$(confirm_exit &)
		if [[ $ans == "yes" || $ans == "YES" || $ans == "y" || $ans == "Y" ]]; then
			kill $(pidof Hyprland)
		elif [[ $ans == "no" || $ans == "NO" || $ans == "n" || $ans == "N" ]]; then
			exit 0
        else
			msg
        fi
        ;;
esac
